import pandas as pd
from ml_elements import get_dataset
from classifier import classifier
import awsgi
from flask import Flask, jsonify, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
app.config["CORS_HEADERS"] = "Content-Type"

@app.route("/")
def index():
	return "<h1>Hello Flask</h1>"

@app.route("/data", methods=["GET"])
def data():
	validation_data = get_dataset()
	return jsonify(validation_data)

@app.route("/predict", methods=["POST"])
def predict_class():
    data = request.get_json()
    df = pd.DataFrame.from_dict(data, orient='index').T
    result = classifier(df)
    print(result)
    return jsonify({"Prediction": result})


def lambda_handler(event, context):
    return awsgi.response(app, event, context, base64_content_types={"image/png"})

# if __name__ == '__main__':
# 	app.run(debug=True)
	