import pandas as pd
from ml_elements import get_model

def classifier(dataset):
    # Get prediction from the pipeline
    model_pipeline = get_model()
    result = model_pipeline.predict(dataset)

    # Reverse encoding to display the result
    if result == 0:
        result = "Placed"
    elif result == 1:
        result = "Not Placed"
    else:
        result = "Error on prediction"

    return result

# Only for debug >> Testing de predictor
# data = {
#     "degree_p": "73.3",
#     "degree_t": "Comm&Mgmt",
#     "etest_p": "91.34",
#     "gender": "F",
#     "hsc_b": "Central",
#     "hsc_p": "52",
#     "hsc_s": "Science",
#     "mba_p": "57.8",
#     "sl_no": "3",
#     "specialisation": "Mkt&Fin",
#     "ssc_b": "Central",
#     "ssc_p": "65",
#     "workex": "Yes"
# }

# df = pd.DataFrame.from_dict(data, orient='index').T
# result = predict(df)
# print(result)