# Web stuff
flask==3.0.2
flask_cors==3.0.10
requests==2.31.0
Werkzeug==3.0.1
pillow==10.2.0
Jinja2==3.1.3

# Data Science stack
pandas==1.5.1
numpy==1.26.4
scikit-learn==1.3.0

# Cloud Stuff
boto3==1.34.56
async_lru==2.0.4
aws-wsgi==0.2.7
gunicorn==21.2.0
python-dotenv
