# Importing modules
import io
import os
import json
import boto3
import joblib
import pandas as pd

def get_model():
    # AWS S3 config
    aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID')
    aws_secret_access_key = os.getenv('AWS_SECRET_ACCESS_KEY')

    s3 = boto3.client(
        service_name = "s3",
        region_name = "eu-west-3",
        aws_access_key_id = aws_access_key_id,
        aws_secret_access_key = aws_secret_access_key,
    )

    # Bucket informations
    bucket_name = "mlelements"
    prefix_model = "models/"
    filename_model = "tuned_piq_model.pkl"

    # Getting the model
    result_model = s3.list_objects(Bucket=bucket_name)
    for obj in result_model.get('Contents'):
        if (obj["Key"].startswith(prefix_model)) and (obj["Key"].endswith(filename_model)):
            model = s3.get_object(Bucket=bucket_name, Key=obj.get('Key'))
            model_content = model['Body'].read()
            model_pipeline = joblib.load(io.BytesIO(model_content))
            return model_pipeline

def get_dataset():
    # AWS S3 config
    aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID')
    aws_secret_access_key = os.getenv('AWS_SECRET_ACCESS_KEY')

    s3 = boto3.client(
        service_name = "s3",
        region_name = "eu-west-3",
        aws_access_key_id = aws_access_key_id,
        aws_secret_access_key = aws_secret_access_key,
    )

    # Bucket informations
    bucket_name = "mlelements"
    prefix_data = "datasets/"
    filename_train = "campus_selection.csv"

    # Getting the dataset
    result_model = s3.list_objects(Bucket=bucket_name)
    for obj in result_model.get('Contents'):
        if (obj["Key"].startswith(prefix_data)) and (obj["Key"].endswith(filename_train)):
            data = s3.get_object(Bucket=bucket_name, Key=obj.get('Key'))
            contents = data['Body'].read().decode("utf-8")
            dataset = pd.read_csv(io.StringIO(contents), low_memory=False)
            
    # for the endpoint API >> form display
    columns = list(dataset.columns)
    columns.remove("status")
    unique_values = {}
    for column in columns:
        unique_values[column] = dataset[column].unique().tolist()
    return unique_values
