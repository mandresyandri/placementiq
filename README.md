# Portfolio 1 : Campus placement prediction
> Welcome to my first machine learning portfolio ! You'll see here the process from data selection to model deployment on a single page website. 

You'll see the app deployed here at [www.placementiq.mandresyandri.fr](http://www.placementiq.mandresyandri.fr/)

## Step 1 : Model build
### 1.1 - [PLACEMENTIQ1](model_build/PLACEMENTIQ1.ipynb) - [Data exploration and validation dataset making](model_build/PLACEMENTIQ1.ipynb)
Data preparation : clean if need, split in train and validation and save those in `AWS S3`. 

### 1.2 - [PLACEMENTIQ2](model_build/PLACEMENTIQ2.ipynb) - [Model selection](model_build/PLACEMENTIQ2.ipynb)
Creating three data `pipelines` (data preprocessing, model), selecting the best model that fit more with this dataset by using `cross validation` save the best model in `AWS S3`. 

### 1.3 - [PLACEMENTIQ3](model_build/PLACEMENTIQ3.ipynb) - [Hyperparameters tuning](model_build/PLACEMENTIQ3.ipynb)
Load the dataset from `AWS S3`, finetune this model with `grid search` strategy and save this in `AWS S3`. 

## Step 2 : Web development
### 1.1 - [Flask app](backend/) - [Backend part](backend/)
Model deployment by building API in `Flask` based on the model saved on `AWS S3`. 

### 1.2 - [React app](frontend/) - [Frontend part](frontend/)
Building single page in `React.js` to allow users use this model in a web page. 

## Step 3 : Deployment 
- The frontend is deployed in a `AWS S3` bucket. 
- The backend is deployed on `Render`

> You can give me some feedback at mandresy.andriantsoanavalona@gmail.com