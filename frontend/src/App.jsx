import "./App.css";
import React, { useState, useEffect } from "react";
import ReactModal from "react-modal";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationCircle } from '@fortawesome/free-solid-svg-icons';
import { faArrowUpRightFromSquare } from '@fortawesome/free-solid-svg-icons';
import { faHourglassStart } from '@fortawesome/free-solid-svg-icons';
import Sign from "./Sign";


function App() {
    const [data, setData] = useState([]);
    const [formState, setFormState] = useState({});
    const [prediction, setPrediction] = useState(null);
    const [sl_no, setSl_no] = useState(1);
    const [touched, setTouched] = useState({});

    const numericFields = ["degree_p", "etest_p", "hsc_p", "mba_p", "ssc_p"];
    const fieldMapping = {
        "Gender": "gender",
        "Secondary Education Percentage": "ssc_p",
        "Secondary Education Board": "ssc_b",
        "Higher Secondary Education Percentage": "hsc_p",
        "Higher Secondary Education Board": "hsc_b",
        "Higher Secondary Education Stream": "hsc_s",
        "Undergraduate Degree Percentage": "degree_p",
        "Undergraduate Degree Type": "degree_t",
        "Work Experience": "workex",
        "Employability Test Percentage": "etest_p",
        "MBA Percentage": "mba_p",
        "Specialization": "specialisation",
    };
    const [isDataLoading, setIsDataLoading] = useState(false);

    useEffect(() => {
        setIsDataLoading(true);
        fetch('https://placementiq.onrender.com/data')
        .then(response => response.json())
        .then(data => {
            const { sl_no, ...otherData } = data;
            setData(otherData);
            setFormState(Object.keys(otherData).reduce((obj, key) => ({ ...obj, [key]: "" }), {}));
            setIsDataLoading(false);
        })
        .catch((error) => {
            console.error("Error:", error);
            setIsDataLoading(false);
        });
    }, []);

    const handleChange = (event) => {
        setFormState({
        ...formState,
        [event.target.name]: event.target.value
        });
    };
    ReactModal.setAppElement('#root');

    const [isLoading, setIsLoading] = useState(false);

    const handleSubmit = (event) => {
        event.preventDefault();
        setIsLoading(true);
        console.log(formState);
        // Envoi des données du formulaire à l'API
        fetch("https://placementiq.onrender.com/predict", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ ...formState, sl_no }), 
        })
        .then(response => response.json())
        .then(data => {
            // console.log("Success:", data);
            setPrediction(data["Prediction"]);
            setSl_no(sl_no + 1); 
            setIsLoading(false);
        })
        .catch((error) => {
            console.error("Error:", error);
            setIsLoading(false);
        });
    };

    const [modalIsOpen, setModalIsOpen] = useState(false);
    useEffect(() => {
        if (prediction) {
            setModalIsOpen(true);
        }
    }, [prediction]);

    const resetForm = () => {
        setFormState(Object.keys(data).reduce((obj, key) => ({ ...obj, [key]: "" }), {}));
    };
    
    const handleBlur = (event) => {
        setTouched({ ...touched, [event.target.name]: true });
    };

    useEffect(() => {
        document.title = "Placement Campus IQ";
        const link = document.createElement('link');
        link.rel = 'apple-touch-icon';
        link.href = '%PUBLIC_URL%/logo192.png';
        document.head.appendChild(link);
    }, []);

    return (
        <div className="App">
            {/* <Helmet>
                <title>Placement Campus IQ</title>
                <link rel="apple-touch-icon" href="%PUBLIC_URL%/logo192.png" />
            </Helmet> */}
            <div className="main-content">
            {/* Content elements */}
                <h1 className="head-title">Placement Campus IQ</h1>
                <p className="presentation-text">
                    My app predicts placement outcomes using the 
                    <a 
                    className="links"
                    target="_blank" rel="noreferrer" 
                    title="More about the dataset"
                    href="https://www.kaggle.com/datasets/meruvulikith/campus-selection-classification-dataset"
                    >
                        &nbsp;Campus Placement Prediction
                        &nbsp;<FontAwesomeIcon icon={faArrowUpRightFromSquare} />
                    </a>
                    &nbsp;dataset by BEN ROSHAN, enabling stakeholders to make informed decisions.
                    This project is realised to enhance my Machine Learning skills. From model building to model deployment. 
                </p>
                <p className="presentation-text">
                    This app will predict if you'll be <strong>"Placed"</strong> or <strong>"Not placed"</strong>. 
                    The <a className="links" 
                    target="_blank" rel="noreferrer" 
                    title="More about the model"
                    href="https://gitlab.com/mandresyandri/placementiq/-/blob/main/model_build/PLACEMENTIQ3.ipynb">Machine Learning Model 
                    &nbsp;<FontAwesomeIcon icon={faArrowUpRightFromSquare} /></a> will make decision based on informations like Undergraduate Degree Percentage, MBA Percentage, Work Experience... (You'll see all bellow)
                </p>
                <p className="info-model">
                    <FontAwesomeIcon className="icon-why" icon={faExclamationCircle}/>&nbsp;
                    <strong>
                        The predictions are based on the dataset published by BEN ROSHAN on Kaggle. This is not generalized model for all campus selection.
                    </strong>
                </p>
            
                {/* Form elements */}
                <section id="prediction-form">
                    
                    <form onSubmit={handleSubmit} className="base-form">
                        <fieldset className="form">
                            <legend>
                                <h2>Let's try the predictor</h2>
                            </legend>
                            {isDataLoading ? (
                                <p className="wait">
                                    <FontAwesomeIcon className="icon-wait" icon={faHourglassStart} />&nbsp;
                                    <strong>
                                    This project is hosted on a free server, which may result in occasional delays in loading, particularly if the app has been inactive for a while. Please be patient and allow a few minutes for the content to load.
                                    </strong> 
                                </p>
                            ) : (
                                Object.keys(formState).map((key) => {
                                    const displayKey = Object.keys(fieldMapping).find(k => fieldMapping[k] === key) || key;
                                    return (
                                        <div className="form-element" key={key}>
                                            <label htmlFor={key}>{displayKey} : </label>
                                            {numericFields.includes(key) ? (
                                                <>
                                                    <input type="number" id={key} name={key} value={formState[key]} onChange={handleChange} onBlur={handleBlur} />
                                                    {formState[key] === "" && touched[key] && <div className="error">Required element</div>}
                                                </>
                                            ) : (
                                                <>
                                                    <select id={key} name={key} value={formState[key]} onChange={handleChange} onBlur={handleBlur}>
                                                        <option value="">Your choice</option>
                                                        {data[key].map((option, index) => (
                                                            <option key={index} value={option}>{option}</option>
                                                        ))}
                                                    </select>
                                                    {formState[key] === "" && touched[key] && <div className="error">Required element</div>}
                                                </>
                                            )}
                                        </div>
                                    );
                                })
                            )}
                            <div className="button-container">
                                <button type="submit" className="form-button">Submit</button>
                            </div>
                        </fieldset>
                        
                        {/* Display Modal element */}
                        <ReactModal
                            className="loadingModal"
                            isOpen={isLoading}
                            contentLabel="Loading">
                                <h2>Waiting for model answer</h2>
                                <div className="spinner"></div>
                        </ReactModal>

                        <ReactModal
                            className="resultModal"
                            isOpen={modalIsOpen && !isLoading}
                            onRequestClose={() => setModalIsOpen(false)}
                            contentLabel="Prediction Result">
                            <>
                                <h2>Prediction result</h2>
                                <p>Based on your informations</p>
                                <p>you'll <span className="result">{prediction}</span></p>
                                <button onClick={() => {
                                    setModalIsOpen(false);
                                    resetForm();
                                    window.location.reload();
                                }}>Close</button>
                            </>
                        </ReactModal>
                    </form>
                </section>
            </div>
            <Sign/>
        </div>
  );
}

export default App;
