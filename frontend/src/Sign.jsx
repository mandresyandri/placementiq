import "./sign.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';

const Sign = () => {
    return (
        <>
            <div className="sign-mandresy">Made with <FontAwesomeIcon icon={faHeart} /> by <a href="http://www.mandresyandri.fr/" className="links">Mandresy Andri</a> ©2024 All rights reserved.</div>
        </>
    )
}

export default Sign;
